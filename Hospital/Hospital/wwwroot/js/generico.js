﻿function modalGuargar(titulo = 'Esta seguro de Guardar?', texto = "Si desea guardar clic en Si " ) {
    return Swal.fire(
        {
        title: titulo,
        text: texto ,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
            confirmButtonText: 'Si'

        }
    )
}