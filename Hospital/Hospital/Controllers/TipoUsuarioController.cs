﻿using Hospital.Clases;
using Hospital.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.Controllers
{
    public class TipoUsuarioController : Controller
    {
        public IActionResult Index(TipoUsuarioCLS objTipoUsuarioCLS)
        {
            List<TipoUsuarioCLS> ListaTipoUsuarioCLS = new();
            using (BDHospitalContext db = new())
            {
                ListaTipoUsuarioCLS = (from tipoUsuario in db.TipoUsuarios
                                       where tipoUsuario.Bhabilitado == 1
                                       select new TipoUsuarioCLS
                                       {
                                           Iidtipousuario = tipoUsuario.Iidtipousuario,
                                           Nombre = tipoUsuario.Nombre,
                                           Descripcion = tipoUsuario.Descripcion

                                       }).ToList();
                if (objTipoUsuarioCLS.Nombre == null && objTipoUsuarioCLS.Descripcion == null && objTipoUsuarioCLS.Iidtipousuario == 0)
                {
                    ViewBag.Nombre = "";
                    ViewBag.Descripcion = "";
                    ViewBag.IidTipoUsuario = 0;
                }
                else
                {
                    if (objTipoUsuarioCLS.Nombre != null)
                    {
                        ListaTipoUsuarioCLS = ListaTipoUsuarioCLS.Where(p => p.Nombre.Contains(objTipoUsuarioCLS.Nombre)).ToList();
                    }
                    if (objTipoUsuarioCLS.Descripcion != null)
                    {
                        ListaTipoUsuarioCLS = ListaTipoUsuarioCLS.Where(p => p.Descripcion.Contains(objTipoUsuarioCLS.Descripcion)).ToList();
                    }
                    if (objTipoUsuarioCLS.Iidtipousuario != 0)
                    {
                        ListaTipoUsuarioCLS = ListaTipoUsuarioCLS.Where(p => p.Iidtipousuario == objTipoUsuarioCLS.Iidtipousuario).ToList();
                    }
                    ViewBag.Nombre = objTipoUsuarioCLS.Nombre;
                    ViewBag.Descripcion = objTipoUsuarioCLS.Descripcion;
                    ViewBag.IidTipoUsuario = objTipoUsuarioCLS.Iidtipousuario;
                }
            }

            return View(ListaTipoUsuarioCLS);
        }
        public IActionResult Agregar()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Agregar(TipoUsuarioCLS objTipoUsuarioCLS)
        {

            try
            {
                using (BDHospitalContext db = new BDHospitalContext())
                {
                    if (!ModelState.IsValid)
                    {
                        return View(objTipoUsuarioCLS);
                    }
                    else
                    {
                        TipoUsuario objTipoUsuario = new();
                        objTipoUsuario.Bhabilitado = 1;
                        objTipoUsuario.Nombre = objTipoUsuarioCLS.Nombre;
                        objTipoUsuario.Descripcion = objTipoUsuarioCLS.Descripcion;
                        db.TipoUsuarios.Add(objTipoUsuario);
                        db.SaveChanges();

                    }
                }
            }
            catch (Exception ex)
            {
                return View(objTipoUsuarioCLS);
            }
            return RedirectToAction("Index");
        }
    }
}
