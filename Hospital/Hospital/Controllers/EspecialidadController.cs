﻿using Hospital.Clases;
using Hospital.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.Controllers
{
    public class EspecialidadController : Controller
    {
        public IActionResult Index(EspecialidadCLS objEspecialidadCLS)
        {
            List<EspecialidadCLS> listEspecialidad = new List<EspecialidadCLS>();
            using (BDHospitalContext db= new BDHospitalContext())
            {
                if(objEspecialidadCLS.nombre == null || objEspecialidadCLS.nombre== "") {
                listEspecialidad = (from especialidad in db.Especialidads
                                    where especialidad.Bhabilitado == 1
                                    select new EspecialidadCLS
                                    {
                                        idEspecialidad = especialidad.Iidespecialidad,
                                        nombre = especialidad.Nombre,
                                        descripcion = especialidad.Descripcion
                                    }).ToList();
                    ViewBag.nombreEspecialidad = "";
                }
                else
                {
                    listEspecialidad = (from especialidad in db.Especialidads
                                        where especialidad.Bhabilitado == 1
                                        && especialidad.Nombre.Contains(objEspecialidadCLS.nombre)
                                        select new EspecialidadCLS
                                        {
                                            idEspecialidad = especialidad.Iidespecialidad,
                                            nombre = especialidad.Nombre,
                                            descripcion = especialidad.Descripcion
                                        }).ToList();
                    ViewBag.nombreEspecialidad = objEspecialidadCLS.nombre;
                }               
            }
            return View(listEspecialidad);
        }

        public IActionResult Agregar()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Agregar(EspecialidadCLS objEspecialidadCLS)
        {
            try 
            {
                using (BDHospitalContext db = new())
                {
                    if (!ModelState.IsValid)
                    {
                        return View(objEspecialidadCLS);
                    }
                    else 
                    {
                        Especialidad objEspecialidad = new Especialidad();
                        objEspecialidad.Nombre = objEspecialidadCLS.nombre;
                        objEspecialidad.Descripcion = objEspecialidadCLS.descripcion;
                        objEspecialidad.Bhabilitado = 1;
                        db.Especialidads.Add(objEspecialidad);
                        db.SaveChanges();
                    }
            }
            }
            catch
            {
                return View(objEspecialidadCLS);
            }
         

                return RedirectToAction("Index");
        }

    }
}
