﻿using Hospital.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.Controllers
{
    public class SedeController : Controller
    {
        public IActionResult Index(SedeCLS objSedeCLS)
        {
            List<SedeCLS> listSedeCLS = new();
            using (BDHospitalContext db = new())
            {
                if(objSedeCLS.nombre == "" ||objSedeCLS.nombre == null)
                {
                    listSedeCLS = (from sede in db.Sedes
                                   where sede.Bhabilitado == 1
                                   select new SedeCLS
                                   {
                                       idSede = sede.Iidsede,
                                       nombre = sede.Nombre,
                                       direccion = sede.Direccion

                                   }).ToList();
                    ViewBag.nombreSedeCLS = "";
                }
                else 
                {
                    listSedeCLS = (from sede in db.Sedes
                                   where sede.Bhabilitado == 1
                                   && sede.Nombre.Contains(objSedeCLS.nombre)
                                   select new SedeCLS
                                   {
                                       idSede = sede.Iidsede,
                                       nombre = sede.Nombre,
                                       direccion = sede.Direccion
                                   }).ToList();
                    ViewBag.nombreSedeCLS = objSedeCLS.nombre;                }

            }
                return View(listSedeCLS);
        }
    }
}
