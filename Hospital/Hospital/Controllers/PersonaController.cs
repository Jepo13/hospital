﻿using Hospital.Clases;
using Hospital.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.Controllers
{
    public class PersonaController : Controller
    {
        public void LlenarSexo()
        {
            List<SelectListItem> listaSexo = new();
            using (BDHospitalContext db = new BDHospitalContext())
            {
                listaSexo = (from sexo in db.Sexos
                             where sexo.Bhabilitado == 1
                             select new SelectListItem
                             {
                                 Value = sexo.Iidsexo.ToString(),
                                 Text = sexo.Nombre
                             }).ToList();
                listaSexo.Insert(0, new SelectListItem
                {
                    Value = "",
                    Text = "--Inserte sexo "

                });
            }
            ViewBag.listaSexo = listaSexo;
        }
        public IActionResult Index(PersonaCLS objPersonaCLS)
        {
            List<PersonaCLS> listPersonaCLS = new List<PersonaCLS>();
            LlenarSexo();
            using (BDHospitalContext db = new BDHospitalContext())
            {
                if (objPersonaCLS.iidSexo == null ||objPersonaCLS.iidSexo == 0)
                {
                    listPersonaCLS = (from persona in db.Personas
                                      join sexo in db.Sexos
                                      on persona.Iidsexo equals sexo.Iidsexo
                                      where persona.Bhabilitado == 1
                                      select new PersonaCLS
                                      {
                                          idPersonas = persona.Iidpersona,
                                          nombreCompleto = persona.Nombre + " " + persona.Appaterno + " " + persona.Apmaterno,
                                          email = persona.Email,
                                          sexo = sexo.Nombre

                                      }).ToList();
                }
                else
                {

                    listPersonaCLS = (from persona in db.Personas
                                      join sexo in db.Sexos
                                      on persona.Iidsexo equals sexo.Iidsexo
                                      where persona.Bhabilitado == 1
                                      && persona.Iidsexo == objPersonaCLS.iidSexo
                                      select new PersonaCLS
                                      {
                                          idPersonas = persona.Iidpersona,
                                          nombreCompleto = persona.Nombre + " " + persona.Appaterno + " " + persona.Apmaterno,
                                          email = persona.Email,
                                          sexo = sexo.Nombre

                                      }).ToList();


                }
            }
            return View(listPersonaCLS);
        }

        public IActionResult Agregar()
        {
            LlenarSexo();
            return View();
        }

        [HttpPost]
        public IActionResult Agregar(PersonaCLS objPersonaCLS)
        {
            LlenarSexo();
            try
            {
                using (BDHospitalContext db = new BDHospitalContext())
                {
                    if (!ModelState.IsValid)
                    {
                        return View(objPersonaCLS);
                    }
                    else
                    {
                        Persona objPersona = new();
                        objPersona.Nombre = objPersonaCLS.nombre;
                        objPersona.Appaterno = objPersonaCLS.apPaterno;
                        objPersona.Apmaterno = objPersonaCLS.apMaterno;
                        objPersona.Telefonofijo = objPersonaCLS.telefonoFijo;
                        objPersona.Telefonocelular = objPersonaCLS.telefonoCelular;
                        objPersona.Fechanacimiento = objPersonaCLS.fechaNacimiento;
                        objPersona.Email = objPersonaCLS.email;
                        objPersona.Iidsexo = objPersonaCLS.iidSexo;
                        objPersona.Bhabilitado = 1;
                        db.Personas.Add(objPersona);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                return View(objPersonaCLS);
            }

            return RedirectToAction("Index");
        }
    }
}
