﻿using Hospital.Clases;
using Hospital.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.Controllers
{
    public class PaginaController : Controller
    {
        public IActionResult Index(PaginaCLS objPaginaCLS)
        {
            List<PaginaCLS> listPaginaCLS = new List<PaginaCLS>();
            using (BDHospitalContext db = new())
            {
                if (objPaginaCLS.mensaje == null || objPaginaCLS.mensaje == "")
                {
                    listPaginaCLS = (from pagina in db.Paginas
                                     where pagina.Bhabilitado == 1
                                     select new PaginaCLS
                                     {
                                         idPagina = pagina.Iidpagina,
                                         mensaje = pagina.Mensaje,
                                         accion = pagina.Accion,
                                         controlador = pagina.Controlador
                                     }).ToList();
                    ViewBag.mensajePagina = "";
                }
                else 
                {
                    listPaginaCLS = (from pagina in db.Paginas
                                     where pagina.Bhabilitado == 1
                                     && pagina.Mensaje.Contains(objPaginaCLS.mensaje)
                                     select new PaginaCLS
                                     {
                                         idPagina = pagina.Iidpagina,
                                         mensaje = pagina.Mensaje,
                                         accion = pagina.Accion,
                                         controlador = pagina.Controlador
                                     }).ToList();
                    ViewBag.mensajePagina = objPaginaCLS.mensaje;
                }
              
            }
                return View(listPaginaCLS);
        }
        public IActionResult Agregar()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Agregar(PaginaCLS objPaginaCLS)
        {
            try
            {
                using (BDHospitalContext db = new())
                {
                    if (!ModelState.IsValid)
                    {
                        return View(objPaginaCLS);

                    }
                    else
                    {
                        Pagina objPagina = new Pagina();
                        objPagina.Bhabilitado = 1;
                        objPagina.Mensaje = objPaginaCLS.mensaje;
                        objPagina.Controlador = objPaginaCLS.controlador;
                        objPagina.Accion = objPaginaCLS.accion;
                       
                        db.Paginas.Add(objPagina);
                        db.SaveChanges();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                return View(objPaginaCLS);
            }
            return RedirectToAction("Index");
        }

    }
}

