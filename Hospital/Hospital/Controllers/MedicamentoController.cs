﻿using Hospital.Clases;
using Hospital.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.Controllers
{
    public class MedicamentoController : Controller
    {
        public List<SelectListItem> LlenarFormaFarmaceutica()
        {
            List<SelectListItem> listaFormaFarmaceutica = new();
            using (BDHospitalContext db = new BDHospitalContext())
            {
                listaFormaFarmaceutica = (from formaFarmaceutica in db.FormaFarmaceuticas
                                          where formaFarmaceutica.Bhabilitado == 1
                                          select new SelectListItem
                                          {
                                              Text = formaFarmaceutica.Nombre,
                                              Value = formaFarmaceutica.Iidformafarmaceutica.ToString()
                                          }).ToList();
                listaFormaFarmaceutica.Insert(0, new SelectListItem
                {
                    Text = "--Todos--",
                    Value = ""
                }
                );
            }
            return listaFormaFarmaceutica;
        }

        public IActionResult Index(MedicamentosCLS objMedicamentosCLS)
        {
            ViewBag.listaFormaFarmaceutica = LlenarFormaFarmaceutica();
            List<MedicamentosCLS> listMedicamentoCls = new();
            using (BDHospitalContext db = new())
            {
                if (objMedicamentosCLS.iidFormaFarmaceutica == null || objMedicamentosCLS.iidFormaFarmaceutica == 0)
                {
                    listMedicamentoCls = (from medicamento in db.Medicamentos
                                          join formaFarmaceutica in db.FormaFarmaceuticas
                                          on medicamento.Iidformafarmaceutica equals formaFarmaceutica.Iidformafarmaceutica
                                          where medicamento.Bhabilitado == 1
                                          select new MedicamentosCLS
                                          {
                                              idMedicamento = medicamento.Iidmedicamento,
                                              nombreMedicamento = medicamento.Nombre,
                                              concentracionMedicamento = medicamento.Concentracion,
                                              formaFamaceutica = formaFarmaceutica.Nombre,
                                              precio = (decimal)medicamento.Precio,
                                              stock = (int)medicamento.Stock,
                                              presentacion = medicamento.Presentacion
                                          }).ToList();
                }
                else
                {
                    listMedicamentoCls = (from medicamento in db.Medicamentos
                                          join formaFarmaceutica in db.FormaFarmaceuticas
                                          on medicamento.Iidformafarmaceutica equals formaFarmaceutica.Iidformafarmaceutica
                                          where medicamento.Bhabilitado == 1
                                          && medicamento.Iidformafarmaceutica == objMedicamentosCLS.iidFormaFarmaceutica
                                          select new MedicamentosCLS
                                          {
                                              idMedicamento = medicamento.Iidmedicamento,
                                              nombreMedicamento = medicamento.Nombre,
                                              concentracionMedicamento = medicamento.Concentracion,
                                              formaFamaceutica = formaFarmaceutica.Nombre,
                                              precio = (decimal)medicamento.Precio,
                                              stock = (int)medicamento.Stock,
                                              presentacion = medicamento.Presentacion
                                          }).ToList();
                }

            }
            return View(listMedicamentoCls);
        }
        public IActionResult Agregar()
        {
            ViewBag.listaFormaFarmaceutica = LlenarFormaFarmaceutica();

            return View();
        }
        [HttpPost]
        public IActionResult Agregar(MedicamentosCLS objMedicamentoCLS)
        {
            try
            {
                using (BDHospitalContext db = new())
                {
                    if (!ModelState.IsValid)
                    {
                        ViewBag.listaFormaFarmaceutica = LlenarFormaFarmaceutica();
                        return View(objMedicamentoCLS);

                    }
                    else
                    {
                        Medicamento objMedicamento = new Medicamento();
                        objMedicamento.Nombre = objMedicamentoCLS.nombreMedicamento;
                        objMedicamento.Precio = objMedicamentoCLS.precio;
                        objMedicamento.Presentacion = objMedicamentoCLS.presentacion;
                        objMedicamento.Stock = objMedicamentoCLS.stock;
                        objMedicamento.Iidformafarmaceutica = objMedicamentoCLS.iidFormaFarmaceutica;
                        objMedicamento.Concentracion = objMedicamentoCLS.concentracionMedicamento;
                        objMedicamento.Bhabilitado = 1;
                        db.Medicamentos.Add(objMedicamento);
                        db.SaveChanges();                       
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.listaFormaFarmaceutica = LlenarFormaFarmaceutica();
                return View(objMedicamentoCLS);

            }

            return RedirectToAction("Index");
        }
    }
}
