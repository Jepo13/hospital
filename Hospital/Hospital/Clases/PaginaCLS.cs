﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.Clases
{
    public class PaginaCLS
    {
        [Display(Name ="Id Página")]
        public int idPagina { get; set; }
        [Display(Name ="Mensaje")]
        [Required(ErrorMessage ="Mensaje es Requerido")]
        public string mensaje { get; set; }
       
        [Display(Name = "Acción")]
        [Required(ErrorMessage = "Accion es un campo obligatorio")]
        public string accion { get; set; }
        [Display(Name = "Controlador")]
        [Required(ErrorMessage ="Controlador es Requerido")]
        [MinLength(3,ErrorMessage ="La longitud minima es de 3")]
        [MaxLengthAttribute(100,ErrorMessage ="La longitud maxima es de 50")]
        public string controlador { get; set;  }
    }
}
