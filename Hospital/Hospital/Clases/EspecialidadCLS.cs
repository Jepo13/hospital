﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.Clases
{
    public class EspecialidadCLS
    {
        [Display (Name ="Id Especialiedad")]
        public int idEspecialidad {get; set;}
        [Display (Name = "Nombre Especialidad")]
        [Required(ErrorMessage ="Ingrese el nombre de la especialidad.")]
        public string nombre {get; set;}
        [Display (Name = "Descripción")]
        [Required (ErrorMessage ="Ingrese una descripción.")]
        public string descripcion { get; set;}
    }
}
