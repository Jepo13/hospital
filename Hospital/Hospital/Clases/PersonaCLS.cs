﻿ using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.Clases
{
    public class PersonaCLS
    {
        [Display(Name = "Id Persona")]
        public int idPersonas { get; set; }

        // Datos para crear 
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Ingrese un nombre.")]
        public string nombre { get; set; }
        [Display(Name = "Apellido Paterno")]
        [Required(ErrorMessage = "Ingrese un apellido paterno.")]
        public string apPaterno { get; set; }

        [Display(Name = "Apellido Materno")]
        [Required(ErrorMessage = "Ingrese un apellido materno.")]
        public string apMaterno { get; set; }
        [Required(ErrorMessage ="Debe ingresar el numero telefonico.")]
        [MinLength(8,ErrorMessage ="Longitud minima de 8 caracteres.")]
        [Display(Name = "Telefono Fijo")]
        public string telefonoFijo { get; set; }

        [Display(Name = "Telefono celular")]
        public string telefonoCelular { get; set; }

        [DataType(DataType.Date,ErrorMessage ="El formato de fecha no es correcto")]
        [Display(Name = "Fecha de nacimiento")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Debe ingresar fecha de nacimiento")]
        public DateTime? fechaNacimiento { get; set; }
       
    
        [Display(Name = "Nombre Completo")]
        public string nombreCompleto { get; set; }
        [Display (Name ="E-mail")]
        [DataType(DataType.EmailAddress,ErrorMessage="El correo no es Valido")]
        public string email { get; set; }
        [Display (Name = "Sexo")]
        public string sexo { get; set; }

        // union con tabla Sexo 
        [Required(ErrorMessage = "Ingrese un sexo")]
        [Display(Name = "Seleccione una opción")]
        public int? iidSexo { get;  set; }
    }
}
