﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.Controllers
{
    public class SedeCLS
    {
        [Display (Name = "Id Sede")]
        public int idSede { get; set ;}
        [Display (Name = "Nombre Sede")]
        public string  nombre { get; set; }
        [Display (Name = "Direción Sede")]
        public string direccion { get; set; }
    }
}
