﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.Clases
{
    public class MedicamentosCLS
{
    [Display(Name = "Id Medicamento")]
    public int idMedicamento { get; set; }

    [Display(Name = "Nombre")]
    [Required(ErrorMessage = "Ingrese un nombre para el medicamento ")]
    public string nombreMedicamento { get; set; }

    [Display(Name = "Forma Farmacéutica")]
    public string formaFamaceutica { get; set; }

    [Display(Name = "Seleccione Forma Farmaceutica")]
    [Required(ErrorMessage = "Ingrese Forma Farmaceutica")]
    public int? iidFormaFarmaceutica { get; set; }

    [Display(Name = "Precio")]
    [Required(ErrorMessage = "Ingrese un precio para el medicamento ")]
    public decimal? precio { get; set; }

    [Display(Name = "Stock")]
    [Required(ErrorMessage = "Ingrese un Stock para el medicamento ")]
    [Range (0,10000,ErrorMessage ="Debe tener un rango de 10 a 10000")]
    public int? stock { get; set; }

    [Display(Name = "Presentación ")]
    [DataType(DataType.MultilineText)]
    public string presentacion { get; set; }

    [Display(Name = "Concentración")]
    public string concentracionMedicamento { get; set; }
}
}
