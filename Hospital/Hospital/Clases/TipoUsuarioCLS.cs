﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hospital.Clases
{
    public class TipoUsuarioCLS
    {
        [Display(Name ="Id Tipo Usuario")]
        public int Iidtipousuario { get; set; }
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Ingrese Nombre")]
        public string Nombre { get; set; }
        [Display(Name = "Descripción")]
        [Required(ErrorMessage ="Ingrese una descripción")]
        public string Descripcion { get; set; }
    }
}
